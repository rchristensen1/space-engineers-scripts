// Loops through all Cargo Containers on ship and condenses multiple stacks on similar items into single stacks.
List<IMyTerminalBlock> blocks;
IMyCargoContainer container;

public Program()
{
    blocks = new List<IMyTerminalBlock>();
    GridTerminalSystem.GetBlocksOfType<IMyCargoContainer>(blocks);
}

void Main()
{

    for(int i=blocks.Count - 1; i>=0; i--)
    {
        container = (IMyCargoContainer)blocks[i];
        List<IMyInventoryItem> containerItems = container.GetInventory(0).GetItems();
        GroupInventoryItems(containerItems);
    }

}

private void GroupInventoryItems(List<IMyInventoryItem> containerItems)
{
    for (int i = containerItems.Count - 1; i >= 0; i--)
    {
        IMyInventoryItem item = containerItems[i];
        PlaceWithLikeItem(containerItems, item);
    }
}

private void PlaceWithLikeItem(List<IMyInventoryItem> containerItems, IMyInventoryItem item)
{
    for (int i = containerItems.Count - 2; i >= 0; i--)
    {
        IMyInventoryItem compareItem = containerItems[i];
        if (item.Content.TypeId == compareItem.Content.TypeId)
        {
            IMyInventory cargoContainer = container.GetInventory(0);
            cargoContainer.TransferItemTo(cargoContainer, i);
            break;
        }
    }
}